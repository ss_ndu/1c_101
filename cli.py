from detector import ShapeDetector
from image_handler import ImageHandler


class CLI:
    @staticmethod
    def cli():
        print("This is a shape detector")
        print("Input the name of the image you want to analyze:")
        img_path = "test_images/squares/square1.png"

        pts = ImageHandler().bw_image(img_path)
        detector = ShapeDetector()
        detector.detect_shape(pts)

