import cv2

class ImageHandler:
    WHITE_THRESHOLD = 250
    BLACK_THRESHOLD = 127

    def read_image(self, img):
        return cv2.imread(img)

    def bw_image(self, img):
        img = self.read_image(img)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        (thresh, bw_img) = cv2.threshold(img, self.BLACK_THRESHOLD, self.WHITE_THRESHOLD, cv2.THRESH_BINARY)
        return bw_img