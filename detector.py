import numpy as np
import math
import enum


class Shapes(enum.Enum):
    CIRCLE = 1
    TRIANGLE = 2
    RECTANGLE = 3
    SQUARE = 4

class ShapeDetector:
    IMAGE_DIST_THRESHOLD = 10

    @staticmethod
    def get_angle(a, b, c):
        ang = math.degrees(math.atan2(c[1]-b[1], c[0]-b[0]) - math.atan2(a[1]-b[1], a[0]-b[0]))
        return ang + 360 if ang < 0 else ang

    def filter_dashes(self, bw_img: np.array) -> list[list[int]]:

        points = []
        for i in range(bw_img.shape[0]-1):
            for j in range(bw_img.shape[1]-1):
                if bw_img[i][j] == 0:
                    points.append([i,j])

        arr = points

        ## O(n^2) algorithm to convert dashes to points
        final_points = []
        while True:
            if not len(arr):
                break

            point1 = arr[0]
            new_points = []
            final_points.append(point1)
            for point2 in arr:
                if abs(point1[0]-point2[0]) > self.IMAGE_DIST_THRESHOLD \
                        or abs(point1[1]-point2[1]) > self.IMAGE_DIST_THRESHOLD: # todo: constants?
                    new_points.append(point2)
            arr = new_points

        return final_points

    @staticmethod
    def pt_dist(p1, p2):
        return np.sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2)

    def is_circle(self, pts) -> bool:
        xs = [x[0] for x in pts]
        ys = [x[1] for x in pts]
        avgx = np.average(xs)
        avgy = np.average(ys)
        arr = []
        for p in pts:
            arr.append(np.sqrt((avgx-p[0])**2 + (avgy-p[1])**2))

        return np.std(arr) <= 2 # todo: constants

    @staticmethod
    def find_extremal_points(points):
        minxminy = points[0]
        minxmaxy = points[0]
        maxxminy = points[0]
        maxxmaxy = points[0]

        minx = points[0]
        miny = points[0]
        maxx = points[0]
        maxy = points[0]

        for point in points:
            if point[0] < minx[0]:
                minx = point
            if point[1] < miny[1]:
                miny = point
            if point[0] > maxx[0]:
                maxx = point
            if point[1] > maxy[1]:
                maxy = point

            if point[0] < minxminy[0] and point[1] < minxminy[1]:
                minxminy = point
            if point[0] < minxmaxy[0] or point[1] > minxmaxy[1]:
                minxmaxy = point
            if point[0] > maxxminy[0] or  point[1] < maxxminy[1]:
                maxxminy = point
            if point[0] > maxxmaxy[0] and point[1] > maxxmaxy[1]:
                maxxmaxy = point

        return {
            "min_x,min_y": minxminy,
            "min_x,max_y": minxmaxy,
            "max_x,min_y" : maxxminy,
            "max_x,max_y": maxxmaxy,
            "minx" :  minx,
            "miny" : miny,
            "maxx"  : maxx,
            "maxy" : maxy
        }

    def calculate_angles(self, pts) -> tuple[list,list]:
        angles_1, angles_2 = [], []
        angles_1.append(self.get_angle(pts['min_x,max_y'], pts['max_x,max_y'] , pts['max_x,min_y']))
        angles_1.append(self.get_angle(pts['min_x,min_y'], pts['min_x,max_y'] , pts['max_x,max_y']))
        angles_1.append(self.get_angle(pts['max_x,max_y'], pts['max_x,min_y'] , pts['min_x,min_y']))
        angles_1.append(self.get_angle(pts['max_x,min_y'], pts['min_x,min_y'] , pts['min_x,max_y']))

        angles_2.append(self.get_angle(pts['minx'], pts['maxy'] , pts['maxx']))
        angles_2.append(self.get_angle(pts['maxy'], pts['maxx'] , pts['miny']))
        angles_2.append(self.get_angle(pts['maxx'], pts['miny'] , pts['minx']))
        angles_2.append(self.get_angle(pts['miny'], pts['minx'] , pts['maxy']))

        return angles_1, angles_2

    def is_rect(self, pts):
        angles_1, angles_2 = self.calculate_angles(self.find_extremal_points(pts))
        angles_1_count, angles_2_count = 0, 0

        for angle in angles_1:
            if abs(angle-90) <= 2:
                angles_1_count+=1

        for angle in angles_2:
            if abs(angle-90) <= 2:
                angles_2_count+=1

        return angles_1_count >= 2 or angles_2_count >= 2 # constants ?

    def is_triangle(self,pts):
        return not self.is_circle(pts) and not self.is_rect(pts) # which is alright *IN TERMS OF THIS PROBLEM*

    def detect_shape(self, pts) -> Shapes:
        pts = self.filter_dashes(pts)
        if self.is_circle(pts):
            print("Is circle")
            return Shapes.CIRCLE
        if self.is_rect(pts):
            print("is rectangle")
            return Shapes.RECTANGLE
        else:
            print("is triangle")
            return Shapes.TRIANGLE

    def is_square(self, pts):
        ext = self.find_extremal_points(pts)
        d1 = self.pt_dist(ext["min_x,min_y"], ext["min_x,max_y"])
        d2 = self.pt_dist(ext["min_x,max_y"], ext["max_x,min_y"])
        d3 = self.pt_dist(ext["min_x,max_y"], ext["max_x,min_y"])
        d4 = self.pt_dist(ext["max_x,min_y"], ext["max_x,max_y"])
        distances = [d1,d2,d3,d4]
        print(distances, np.std(distances))
        if np.std(distances) >= 2:
            return np.average(distances)

        #     "minx" :  minx,
        #     "miny" : miny,
        #     "maxx"  : maxx,
        #     "maxy" : maxy



